#! /bin/bash

docker ps -a -q | xargs --no-run-if-empty docker rm
docker images -q --filter dangling=true | xargs --no-run-if-empty docker rmi
