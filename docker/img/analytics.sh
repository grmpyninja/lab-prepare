#! /bin/bash

# Base images
docker pull alpine
docker pull ubuntu/latest
docker pull busybox
docker pull golang

# Analytics stacks
docker pull jupyter/tensorflow-notebook
docker pull jupyter/datascience-notebook

