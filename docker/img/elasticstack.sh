#! /bin/bash

# Base images
docker pull alpine
docker pull ubuntu/latest

# Analytics stacks

#docker pull elasticsearch
docker pull elasticsearch:alpine
#docker pull logstash
docker pull logstash:alpine
docker pull kibana

