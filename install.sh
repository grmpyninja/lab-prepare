#! /bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

apt-get update && apt-get upgrade

# TODO review and remove if necessary
# apt-get install -y -f apt-transport-https ca-certificates locales curl software-properties-common
# apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 \
#             --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

export LANG=en_US.UTF-8
apt-get install -y locales && \
    sed -i -e "s/# $LANG.*/$LANG.UTF-8 UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LANG

# TODO: Fix openstack images, tips here
# https://ask.openstack.org/en/question/61153/sudo-unable-to-resolve-host/


# Install docker, directly from <https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository>
# echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"


# For 16.04
apt-get update
apt-get install -y -f linux-image-extra-$(uname -r) linux-image-extra-virtual
apt-get install -y -f docker-ce

#service docker start
systemctl enable docker

# Insecure config
# add docker group and current user to that group
groupadd docker
usermod -aG docker $USER

